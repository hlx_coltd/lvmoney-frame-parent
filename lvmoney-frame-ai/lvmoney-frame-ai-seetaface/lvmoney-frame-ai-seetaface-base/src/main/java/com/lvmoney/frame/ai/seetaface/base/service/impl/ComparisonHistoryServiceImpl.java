package com.lvmoney.frame.ai.seetaface.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lvmoney.frame.ai.seetaface.base.entity.ComparisonHistory;
import com.lvmoney.frame.ai.seetaface.base.dao.ComparisonHistoryDao;
import com.lvmoney.frame.ai.seetaface.base.service.ComparisonHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 比对历史 服务实现类
 * </p>
 *
 * @author lvmoney
 * @since 2022-02-10
 */
@Service
public class ComparisonHistoryServiceImpl extends ServiceImpl<ComparisonHistoryDao, ComparisonHistory> implements ComparisonHistoryService {

}
